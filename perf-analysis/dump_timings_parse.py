#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from pathlib import Path

def read_timings_log(file: Path) -> pd.DataFrame:
    ms = []
    import re
    cur_mod = None
    pass_n = 0
    for m in re.finditer(r'(.+) \[([a-zA-Z\.]*)\]: alloc=([0-9]*) time=([0-9]*\.[0-9]*)', file.read_text()):
        phase = m.group(1)
        mod = m.group(2)
        alloc = float(m.group(3))
        time = float(m.group(4))
        if mod != cur_mod:
            pass_n = 0
            cur_mod = mod

        ms.append((phase, mod, pass_n, alloc, time))
        pass_n += 1
        
    return pd.DataFrame(ms, columns=['pass', 'module', 'pass_n', 'alloc', 'time'])
